import React from "react";
import { Container, Row, Navbar, NavbarBrand, Nav, NavItem } from "reactstrap";
import notif from "..//img/notifications-bell-button.svg"
import profil from "../img/no-profile-photo.png";

const Navdash = () => {
  return (
    <Navbar className="navdash">
      <Container>
        <Row>
          <NavbarBrand className="fund">
              fund.
          </NavbarBrand>

          <Nav className="notif mr-auto">
            <NavItem>
                <img src={notif} className="picnot" alt="pict" />
            </NavItem>
            <NavItem>
                <img src={profil} className="picprof" alt="pict" />
            </NavItem>
          </Nav>
        </Row>
      </Container>
    </Navbar>
  );
};

export default Navdash;
