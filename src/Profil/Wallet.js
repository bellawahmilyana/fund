import React, {  useState } from "react";
import { InputGroup, InputGroupAddon, InputGroupText, Input, Card, Col, Row, Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Footer from "./Footer";
import icon from "../img/ewallet.svg";

function Wallet(props) {

  const { className } = props;

  const [modal, setModal] = useState(false);
  const [modal2, setModal2] = useState(false);

  const toggle = () => setModal(!modal);
  const toggle2 = () => setModal2(!modal2);

  return (
    <div>

      <Row>

        <Col>
          <Card className="kartu3">
              <h2>E-Wallet</h2>
              <Card className="kardwallet">
                <Row>
                <Col>
                  <h4>Saldo</h4>
                  <h5>Rp.</h5>
                </Col>
                <Col>
                  <h4>Budget Limit</h4>
                  <h5>Rp.</h5>
                </Col>
                </Row>
              </Card>

              <Card className="kardtopup">
                <Row>
                  <Col className="kolom" onClick={toggle}>
                    <img src={icon} className="wallet" alt="pict" />
                    <h6>Top-up</h6>
                  </Col>
                  <Col className="kolom1" onClick={toggle2}>
                    <img src={icon} className="wallet1" alt="pict" />
                    <h6>Set Budget</h6>
                  </Col>
                </Row>
              </Card>
          </Card>

          <Footer />
        </Col>

      </Row>

      <Modal isOpen={modal} fade={false} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Top-Up</ModalHeader>
        <ModalBody>
          <h6>Amount</h6>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Rp.</InputGroupText>
            </InputGroupAddon>
              <Input type="number" placeholder="100000" />
          </InputGroup>
        </ModalBody>
        <ModalFooter>
          <Button type="submit" color="success" onClick={toggle}>Top-Up</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>


      <Modal isOpen={modal2} fade={false} toggle={toggle2} className={className}>
        <ModalHeader toggle={toggle2}>Set Budget Limit</ModalHeader>
        <ModalBody>
          <h6>Amount</h6>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Rp.</InputGroupText>
            </InputGroupAddon>
              <Input type="number" placeholder="100000" />
          </InputGroup>
        </ModalBody>
        <ModalFooter>
          <Button type="submit" color="success" onClick={toggle2}>Set Budget</Button>{' '}
          <Button color="secondary" onClick={toggle2}>Cancel</Button>
        </ModalFooter>
      </Modal>

    </div>
  );
}

export default Wallet;