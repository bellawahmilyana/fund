import React, { } from "react";
import { Card, Col, Row } from "reactstrap";
import { Line } from '@reactchartjs/react-chart.js';
import Footer from "./Footer";

function Dashboard() {

  const data = {
    labels: ['1', '2', '3', '4', '5', '6'],
    datasets: [
      {
        label: 'data',
        data: [0, 2, 3.5, 4, 5, 6],
        fill: true,
        //backgroundColor: 'linear-gradient(180deg, rgba(121, 113, 234, 0.8) 59.26%, rgba(255, 255, 255, 0.536) 100%)',
        backgroundColor: '#7971EA',
        borderColor: '#7971EA',
      },
    ],
  }

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  }

  return (
    <div>

      <Row>

        <Col>
          <Card className="kartu1">
            <div>
              <h2>Last month spending</h2>
                
                <Line className="grafik" data={data} options={options} />
              
            </div>
          </Card>

          <Card className="kartu2">
            <h2>Transaction</h2>
            <Card className="kardetail">
              detail transaction
            </Card>
            <Card className="kardetail">
              detail transaction
            </Card>
          </Card>

          <Footer />
        </Col>

      </Row>

    </div>
  );
}

export default Dashboard;